
// solve the path problem
var webdriver = require(__dirname+'/node_modules/selenium-webdriver');
//var serialize = require('node-serialize');

var chrome = require(__dirname+'/node_modules/selenium-webdriver/chrome');

var opts = new chrome.Options();
opts.addArguments(['user-agent="Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36"']);
opts.addArguments(["test-type"]);
opts.addArguments(["user-data-dir="+__dirname+"/profile"]);
opts.addArguments(["--window-size=800,700"]);
opts.addArguments(["--disable-remote-fonts"]);
opts.addArguments(["--disable-plugins"]);
opts.addArguments(["--allow-running-insecure-content"]);
opts.setUserPreferences({
    'default_content_settings': {'images': 2},
    "managed_default_content_settings.images": 2
//    "default_content_settings.images": 'block'
});
//console.log(opts.excludeSwitches());
//var sss = opts.toCapabilities();
//console.log(Object.keys(opts['options_']['args']));
//console.log(sss);
//var cap = opts.toCapabilities();
//console.log(cap);
//var ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36';
//var chrome = require('selenium-webdriver/chrome');
var path = require(__dirname+'/node_modules/chromedriver').path;
var service = new chrome.ServiceBuilder(path).build();
chrome.setDefaultService(service);
//console.log(path);
//var service = new chrome.ServiceBuilder('/usr/local/bin/chromedriver').loggingTo('log.txt').enableVerboseLogging().build();
//chrome.setDefaultService(service);

var baseUrl = process.argv[process.argv.length - 1];
var domain = baseUrl.match(/(http(s?))\:\/\/(.*)\.com/);
domain = domain[domain.length - 1];
var fs = require('fs');
var list = fs.readdirSync('.');
for (var x = 0; x < list.length; x++) {
    var file = list[x];
    if (file.match(/\.png/)) fs.unlinkSync('.\\' + file);
}


var driver = new webdriver.Builder()
//    .usingServer('http://localhost:4444/wd/hub')
    /*
    .withCapabilities({
        resolution : '800x600',
        "chrome.prefs": {
            "download.default_directory": "/code",
        },
        applicationCacheEnabled:false,
        browserName: 'chrome',
        chromeOptions: {
            args: ['user-agent="Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36"', '-disable-images']
            prefs: {"chrome.prefs": {
                "download.default_directory": "/code",
            }},
        },
    })
    */
    .withCapabilities(opts.toCapabilities())
    //.withCapabilities({browserName: 'chrome', chromeOptions: {options_:{args: ['user-agent="Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36"']} } })
    .build();

    driver.get(baseUrl).then(function () {

        driver.findElement(webdriver.By.className('customButton1_1'))
        .then(function (obj) {
            obj.click();
            setTimeout(function () {
                driver.getAllWindowHandles().then(function (handles) {
                    if (handles.length == 1) {
                        console.log('OK');
                    } else {
                        console.log('redirecting to Chrome store');
                    }
                    driver.quit();
                });
            }, 2000);
        })
        .catch(function (err) {
            console.log('Download clicking error');
            driver.quit();
        });
    });



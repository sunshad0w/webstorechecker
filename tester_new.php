<?php
$p = pcntl_fork();
if ($p===0)
    posix_setsid();
else if ($p===-1)
    die("Cannot daemonize");
else
    exit;

$domains = [
    'http://pconverter.com',
    'http://downspeedtest.com',
    'http://productivityboss.com',
    'http://filesendsuite.com',
    'http://mergedocsonline.com',
    'http://myformsfinder.com',
    'http://everydaymanuals.com',
    'http://convertpdfsnow.com',
    'http://easyfileconvert.com',
    'http://getvideoconvert.com',
];
class domainInfo{
    public $curStatus;
    public $prevStatus = false;
    public $curErrors = 0;
    public $prevErrors = 0;
    public $domainName;
    public $lastCheckTS;

    public static function __set_state($an_array)
    {
        $obj = new domainInfo();
        foreach ($an_array as $key => $item) {
            $obj->$key = $item;
        }
        return $obj;
    }

}


function send($domain, $domainInfo, $additional = '')
{
    if (isset($domain) && isset($domainInfo)){
        $status = $domainInfo->curStatus;
        $times = 4;
        if ($domainInfo->curErrors == 0){
            if ($domainInfo->prevErrors > $times){
                $message = "Hello:\n\n domain: " . $domain . "\nstatus FIXED";
                $subject = '[AutoChecker] URGENT: Fixed error'.$domain.', status - fixed after '.$domainInfo->prevErrors. ' times';
                $headers = 'From: info@goldbar.com' . "\r\n" .
                    'Reply-To: info@goldbar.com' . "\r\n" .
                    'Cc: sinitsyn.slava@gmail.com' . "\r\n" .
                    'X-Mailer: PHP/' . phpversion();
                $headers .= "MIME-Version: 1.0\r\n";
                $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
                $mail = mail('sunshad0w.0leg@gmail.com', $subject, $message, $headers);

                //send OK for as
            }
        } else {
            if ($domainInfo->prevErrors == $times){
                $message = "Hello:\n\n domain: " . $domain . "\nstatus Error";
                $subject = '[AutoChecker] URGENT: Error!'.$domain.', status - Error more '.$times.' times';
                $headers = 'From: info@goldbar.com' . "\r\n" .
                    'Reply-To: info@goldbar.com' . "\r\n" .
                    'Cc: sinitsyn.slava@gmail.com' . "\r\n" .
                    'X-Mailer: PHP/' . phpversion();
                $headers .= "MIME-Version: 1.0\r\n";
                $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
                $mail = mail('sunshad0w.0leg@gmail.com', $subject, $message, $headers);

                //send Error for as
            }
        }

        if ($domainInfo->curStatus !== $domainInfo->prevStatus){
            //send Avi
            $message = "Hello:\n\n domain: " . $domain . "\nstatus " . $status;
            $subject = '[AutoChecker] URGENT: '.$domain.', status - '.$status;
            $headers = 'From: info@goldbar.com' . "\r\n" .
                'Reply-To: info@goldbar.com' . "\r\n" .
                //'Cc: sinitsyn.slava@gmail.com, yaniv.bl@gmail.com, avigoldfinger@gmail.com, parkprog@gmail.com' . "\r\n" .
                'Cc: sinitsyn.slava@gmail.com' . "\r\n" .
                'X-Mailer: PHP/' . phpversion();
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
            $mail = mail('sunshad0w.0leg@gmail.com', $subject, $message, $headers);
        }
    }
}


/**
 * Run process with timeout
 * @param string $command
 * @param int $timeout - sec
 * @param int $sleep
 * @param string $file_out_put - if default value, then return true else return out of process
 * @return bool or str
 */
function PsExecute($command, $timeout = 20, $sleep = 1, $file_out_put = '/dev/null')
{

    $pid = PsExec($command, $file_out_put);

    if ($pid === false) {
        return false;
    }

    $cur = 0;

    // пока не истекло время отведенное на выполнение скрипта продолжаем ждать
    while ($cur < $timeout) {
        sleep($sleep);
        $cur += $sleep;

        if (!PsExists($pid)) {
            // скрипт завершил своё выполнение, можно посмотреть его результат или просто вернуть true
            if ($file_out_put != '/dev/null') {
                return trim(file_get_contents($file_out_put));
            } else {
                return true;
            }
        }
    }

    // не дождались пока звершиться скрипт, по этому автоматически убиваем его
    PsKill($pid);
    return false;
}

/**
 * Run process in background with out buffer to file
 * @param string $commandJob
 * @param string $file_out_put
 * @return int or false
 */
function PsExec($commandJob, $file_out_put)
{
    $command = $commandJob . ' > ' . $file_out_put . ' 2>&1 & echo $!';
    exec($command, $op);
    $pid = (int)$op[0];

    if ($pid != "") return $pid;

    return false;
}

/**
 * If process exists then return true else return false
 * @param int $pid
 * @return bool
 */
function PsExists($pid)
{

    exec("ps ax | grep $pid 2>&1", $output);

    while (list(, $row) = each($output)) {

        $row_array = explode(" ", trim($row));
        $check_pid = $row_array[0];

        if ($pid == $check_pid) {
            return true;
        }

    }

    return false;
}

/**
 * Kill process
 * @param int $pid
 */
function PsKill($pid)
{
    echo "killed\n";
    exec("kill `ps -ef | grep chromium | grep -v grep | awk '{print $2}'`");
    //exec("kill -9 $pid", $output);
}

function toCounter(){
    $data[] = array
    (
        'time' => time(),
        'counter_id' => '1919356662',
        'value' => 1,
    );

    $ch = curl_init('http://goldbar.86400.ru/receiver.php');

    curl_setopt_array($ch, array
    (
        CURLOPT_POST => 1,
        CURLOPT_HEADER => 0,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_POSTFIELDS => array('data' => json_encode($data)),
    ));

    curl_exec($ch);
    curl_close($ch);
}
$i = 1;
$time_start_iterate = 0;
$waitingTime = 40;

while(true) {
    /*
    if (
        ($time_start_iterate != 0) &&
        (time() - $time_start_iterate < 900)
    ) { //sleep till 60sec from prev job start
        sleep(60);
        continue;
    }
    */
    $time_start_iterate = time();

    $prev_statuses = [];
    if(is_file(__DIR__.'/statuses.php')) $prev_statuses = include(__DIR__.'/statuses.php');

    $result = [];
    foreach ($domains as $key => $domain) {
        preg_match('/(http(s?))\:\/\/(.*)\.com/iU', $domain, $match);
        $short_name = $match[count($match) - 1] . ".com";

        $domainInfo = new domainInfo();
        $domainInfo->domainName = $domain;
        $skipDomain = false;
        if (array_key_exists($domain, $prev_statuses)){
            if (isset(($prev_statuses[$domain])->lastCheckTS) && ($prev_statuses[$domain])->lastCheckTS !== null && ($prev_statuses[$domain])->lastCheckTS > (time() - 900)){
                $domainInfo->lastCheckTS = ($prev_statuses[$domain])->lastCheckTS;
                $skipDomain = true;
            } else {
                $domainInfo->lastCheckTS = time();
                echo "$domain set time\n";
            }
            $domainInfo->prevStatus = ($prev_statuses[$domain])->curStatus;
            $domainInfo->prevErrors = ($prev_statuses[$domain])->curErrors;
        } else {
            $domainInfo->lastCheckTS = time();
            echo "$domain set time\n";
        }

        if (!$skipDomain) {
            //echo "run: " . '/usr/bin/node ' . __DIR__ . '/checker.js ' . $domain . "\n";

            $execInfo = PsExecute('/usr/bin/node ' . __DIR__ . '/checker.js ' . $domain, $waitingTime * 2, 1, __DIR__ . '/tmp');
            //var_dump('=================result_file===================');
            //var_dump($execInfo);

            if ($execInfo === true || $execInfo === false || $execInfo === 'Download clicking error' || $execInfo === "undefined") {
                //echo "Error block begin\n";
                //var_dump($domainInfo);
                $domainInfo->curStatus = $domainInfo->prevStatus;
                $domainInfo->curErrors = $domainInfo->prevErrors + 1;
                //var_dump($domainInfo);
                //echo "Error block end\n";
            } elseif (strlen($execInfo) > 100){
                echo var_export($execInfo, true)."\n";
                $domainInfo->curStatus = $domainInfo->prevStatus;
                $domainInfo->curErrors = $domainInfo->prevErrors;
            }else {
                //if ($domainInfo->errors > 0) send($short_name, $domainInfo); //была ошибка
                $domainInfo->curStatus = $execInfo;
                $domainInfo->curErrors = 0;
            }
        } else {
            echo "$domain skipped by time\n";

            $domainInfo->curStatus = $domainInfo->prevStatus;
            $domainInfo->curErrors = $domainInfo->prevErrors;
        }

        if (!$skipDomain) {
            if ($domainInfo->prevStatus == false) { //данных не было
                echo "no previous\n";
                if ($domainInfo->curErrors == 0) { //сейчас не ошибка
                    echo "  not error\n";
                    if ($domainInfo->curStatus !== 'OK') { //сейчас не "ок"
                        echo "    not ok\n";
                        send($short_name, $domainInfo);
                    }
                } else { // сейчас ошибка
                    echo "  error\n";
                    send($short_name, $domainInfo);
                }
            } else { //данные были
                echo "YES previous\n";
                if ($domainInfo->curErrors == 0) { //сейчас не ошибка
                    echo "  not error\n";
                    if ($domainInfo->curStatus !== $domainInfo->prevStatus) { //статус изменился
                        echo "    status changed\n";
                        send($short_name, $domainInfo);
                    } else { //статус не изменился
                        echo "    status still the same\n";
                        send($short_name, $domainInfo);
                    }
                } else { //сейчас ошибка
                    echo "  error\n";
                    send($short_name, $domainInfo);
                }
            }
            //var_dump($domain);
            var_dump($domainInfo);
            echo "==========================\n";
        }
        $result[$domain] = $domainInfo;
    }

    exec('pkill chromium');
    echo "-----------------------round next-----------------------\n";
    $statuses = var_export($result, true);
    file_put_contents(__DIR__.'/statuses.php', "<?php\nreturn " . $statuses . ";");

    //$d = 'iteration ('.$i.') [' . date('d-m-Y H:i:s').']';
    $i++;
    //echo $d . "\n";

    toCounter();

    sleep($waitingTime);
}


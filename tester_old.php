<?php
$p = pcntl_fork();
if ($p===0)
    posix_setsid();
else if ($p===-1)
    die("Cannot daemonize");
else
    exit;

$domains = [
    'http://pconverter.com',
    'http://downspeedtest.com',
    'http://downshotfree.com',
    'http://productivityboss.com',
    'http://myformsfinder.com',
    'http://filesendsuite.com',
    'http://dezipper.com',
    'http://everydaymanuals.com',
    'http://free.flightsearchapp.com',
];

function send($domain, $status, $additional = '')
{
    $status = trim($status);
    if (isset($domain) && $domain !== '' && $status !== 'Download clicking or other error'){
        $message = "Hello:\n\n domain: " . $domain . "\nstatus " . $status;
        $subject = '[AutoChecker] URGENT: '.$domain.', status - '.$status;
        $headers = 'From: info@goldbar.com' . "\r\n" .
            'Reply-To: info@goldbar.com' . "\r\n" .
            //'Cc: sinitsyn.slava@gmail.com, yaniv.bl@gmail.com, avigoldfinger@gmail.com, parkprog@gmail.com' . "\r\n" .
            'Cc: sinitsyn.slava@gmail.com' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        $mail = mail('sunshad0w.0leg@gmail.com', $subject, $message, $headers);
    } else {
        $message = "Hello:\n\n domain: " . $domain . "\nstatus " . $status;
        $subject = '[AutoChecker] URGENT: Error!'.$domain.', status - '.$status;
        $headers = 'From: info@goldbar.com' . "\r\n" .
            'Reply-To: info@goldbar.com' . "\r\n" .
            'Cc: sinitsyn.slava@gmail.com, parkprog@gmail.com' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        $mail = mail('sunshad0w.0leg@gmail.com', $subject, $message, $headers);
        file_put_contents(
            __DIR__.'/errors.log',
            var_export(['domain'=>$domain, 'status'=>$status, 'add'=>$additional], true)."\n".date('d-m-Y H:i:s')."\n\n",
            FILE_APPEND );
    }
}


/**
 * Run process with timeout
 * @param string $command
 * @param int $timeout - sec
 * @param int $sleep
 * @param string $file_out_put - if default value, then return true else return out of process
 * @return bool or str
 */
function PsExecute($command, $timeout = 20, $sleep = 1, $file_out_put = '/dev/null')
{

    $pid = PsExec($command, $file_out_put);

    if ($pid === false) {
        return false;
    }

    $cur = 0;

    // пока не истекло время отведенное на выполнение скрипта продолжаем ждать
    while ($cur < $timeout) {
        sleep($sleep);
        $cur += $sleep;

        if (!PsExists($pid)) {
            // скрипт завершил своё выполнение, можно посмотреть его результат или просто вернуть true
            if ($file_out_put != '/dev/null') {
                return trim(file_get_contents($file_out_put));
            } else {
                return true;
            }
        }
    }

    // не дождались пока звершиться скрипт, по этому автоматически убиваем его
    PsKill($pid);
    return false;
}

/**
 * Run process in background with out buffer to file
 * @param string $commandJob
 * @param string $file_out_put
 * @return int or false
 */
function PsExec($commandJob, $file_out_put)
{
    $command = $commandJob . ' > ' . $file_out_put . ' 2>&1 & echo $!';
    exec($command, $op);
    $pid = (int)$op[0];

    if ($pid != "") return $pid;

    return false;
}

/**
 * If process exists then return true else return false
 * @param int $pid
 * @return bool
 */
function PsExists($pid)
{

    exec("ps ax | grep $pid 2>&1", $output);

    while (list(, $row) = each($output)) {

        $row_array = explode(" ", trim($row));
        $check_pid = $row_array[0];

        if ($pid == $check_pid) {
            return true;
        }

    }

    return false;
}

/**
 * Kill process
 * @param int $pid
 */
function PsKill($pid)
{
    echo "killed\n";
    exec("kill `ps -ef | grep chromium | grep -v grep | awk '{print $2}'`");
    //exec("kill -9 $pid", $output);
}

$i = 1;
$time_start_iterate = 0;

while(true) {
    if (
        ($time_start_iterate != 0) &&
        (time() - $time_start_iterate < 900)
    ) { //sleep till 60sec from prev job start
        sleep(60);
        continue;
    }
    $time_start_iterate = time();

    $prev_statuses = [];
    if(is_file(__DIR__.'/statuses.php')) $prev_statuses = include(__DIR__.'/statuses.php');

    $result = [];
    foreach ($domains as $key => $domain) {
        echo "run: " . 'node ' . __DIR__ . '/checker.js ' . $domain. "\n";
        $result[$domain] = PsExecute('node '.__DIR__.'/checker.js '.$domain, 120, 1, __DIR__.'/tmp');
        sleep(3);
        if (strlen($result[$domain]) > 100 || $result[$domain] === true || $result[$domain] === false) {
            file_put_contents('errors.log', $domain.":\n".$result[$domain], FILE_APPEND);
            $result[$domain] = 'Download clicking or other error';
        }
        preg_match('/(http(s?))\:\/\/(.*)\.com/iU', $domain, $match);
        $short_name = $match[count($match) - 1] . ".com";
        if (array_key_exists($domain, $prev_statuses)){
            if (($prev_statuses[$domain] !== $result[$domain])
            or ($prev_statuses[$domain] == $result[$domain] && $prev_statuses[$domain] == 'Download clicking or other error')){
                send($short_name, $result[$domain]);
            }
        } else if($result[$domain] !== 'ok'){
            send($short_name, $result[$domain]);
        } else {
            //new domain ok
        }
    }

    exec('pkill chromium');
    $statuses = var_export($result, true);
    file_put_contents(__DIR__.'/statuses.php', "<?php\nreturn " . $statuses . ";");

    $d = 'iteration ('.$i.') [' . date('d-m-Y H:i:s').']';
    $i++;
    echo $d . "\n";
}